#!/bin/bash

TMP_FOLDER=$(mktemp -d)
CONFIG_FILE='marcoin.conf'
CONFIGFOLDER='/root/.marcoin'
COIN_DAEMON='marcoind'
COIN_CLI='marcoin-cli'
COIN_PATH='/usr/local/bin/'
COIN_TGZ='https://github.com/MarketArbitrageCoin/MARCresources/raw/master/marcoin-linux.tar.gz'
COIN_ZIP=$(echo $COIN_TGZ | awk -F'/' '{print $NF}')
COIN_NAME='marcoin'
COIN_PORT=44004
RPC_PORT=44005

function download_node() {
  echo -e "Prepare to download ${GREEN}$COIN_NAME${NC}."
  cd $TMP_FOLDER >/dev/null 2>&1
  wget -q $COIN_TGZ
  tar -xvzf $COIN_ZIP >/dev/null 2>&1
  cp bin/marcoin* $COIN_PATH
  chmod 755 /usr/local/bin/*
  cd - >/dev/null 2>&1
  rm -rf $TMP_FOLDER >/dev/null 2>&1
  clear
}


function configure_systemd() {
  systemctl daemon-reload
}

##### Main #####
clear

download_node
configure_systemd
